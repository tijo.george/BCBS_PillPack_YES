package com.perficient.objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.perficient.util.CommonUtilities;

import io.appium.java_client.AppiumDriver;

public class MyBlueAppPageelements  extends CommonUtilities {

	public AppiumDriver driver;
	public MyBlueAppPageelements(AppiumDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath="//*[@text='Create Account']")
	public WebElement btnCreateAccount;
	
	@FindBy(xpath="//*[@text='Password*']")
	public WebElement labelPassword;
	
	@FindBy(xpath="(//*[@class='android.widget.EditText'])[1]")
	public WebElement txtPhone;
	
	@FindBy(xpath="(//*[@class='android.widget.EditText'])[2]")
	public WebElement txtPassword;
	
	@FindBy(xpath="//*[@text='Use email instead']")
	public WebElement linkUseEmail;
	
	@FindBy(xpath="//*[@text='Continue']")
	public WebElement btnContinue;
	
	@FindBy(xpath="//*[@text='Verify Account Now']")
	public WebElement btnVerifyAccountNow;
	
	@FindBy(xpath="(//*[@class='android.widget.EditText'])[1]")
	public WebElement txtFirstName;
	
	@FindBy(xpath="(//*[@class='android.widget.EditText'])[2]")
	public WebElement txtLastName;
	
	@FindBy(xpath="//*[@resource-id='dob']")
	public WebElement txtDOB;
	
	@FindBy(xpath="//*[@resource-id='mobile']")
	public WebElement txtPhoneNumber;
	
	@FindBy(xpath="//*[@text='Select a question']")
	public WebElement listSecurityQuestion;
	
	@FindBy(xpath="//*[@resource-id='alert-input-3-0']")
	public WebElement SecurityQuestion;
		
	@FindBy(xpath="//*[@text='SELECT']")
	public WebElement btnSelect;
	
	
	@FindBy(xpath="//*[@resource-id='hintAnswer']")
	public WebElement txtAnswer;
	
	@FindBy(xpath="//*[@resource-id='memberid']")
	public WebElement txtCardMemId;
	
	@FindBy(xpath="//*[@text='Continue']")
	public WebElement txtSSN;
	
	@FindBy(xpath="//*[@resource-id='mat-input-1']")
	public WebElement txtAccesscode1;
	
	@FindBy(xpath="//*[@resource-id='mat-input-2']")
	public WebElement txtAccesscode2;
	
	@FindBy(xpath="//*[@resource-id='mat-input-3']")
	public WebElement txtAccesscode3;
	
	@FindBy(xpath="//*[@resource-id='mat-input-4']")
	public WebElement txtAccesscode4;
	
	@FindBy(xpath="//*[@resource-id='mat-input-5']")
	public WebElement txtAccesscode5;
	
	@FindBy(xpath="//*[@resource-id='mat-input-6']")
	public WebElement txtAccesscode6;
	
	
	public void performSearch() throws Exception {
		
		click(btnCreateAccount, "Create Acount Button");
		click(linkUseEmail, "Link use email");
		enterText(txtPhone, "testrft@yopmail.com", "Phone Number");
		txtPassword.click();
		enterText(txtPassword, "Password1$", "password");
		labelPassword.click();
		click(btnContinue, "Continue Button");
	}


	public void testUserRegistration(String username, String password, String firstname, String lastname, String dob,
			String crdid, String ssn) throws Exception {
		
		click(btnCreateAccount, "Create Acount Button");
		click(linkUseEmail, "Link use email");
		enterText(txtPhone, username, "Email Address");
		txtPassword.click();
		enterText(txtPassword, password, "password");
		labelPassword.click();
		click(btnContinue, "Continue Button");
		click(btnVerifyAccountNow, "Verify Acount Button");
		enterText(txtFirstName, firstname, "First NAme");
		enterText(txtLastName, lastname, "Last Name");
		enterText(txtDOB, dob, "Date of Birth");
		enterText(txtPhoneNumber, "6171726556", "Phone Number");
		listSecurityQuestion.click();
		click(SecurityQuestion, "Security Question");
		click(btnSelect, "Select Button");
		enterText(txtAnswer, "test", "Answer");
		click(btnContinue, "Continue Button");
		enterText(txtCardMemId, crdid, "Card Member ID");
		click(btnContinue, "Continue Button");
	}
	
		
}

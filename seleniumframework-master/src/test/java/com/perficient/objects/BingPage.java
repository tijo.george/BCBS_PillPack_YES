package com.perficient.objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.perficient.util.CommonUtilities;

public class BingPage  extends CommonUtilities {

	public BingPage(RemoteWebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="sb_form_q")
	public WebElement searchBox;

	@FindBy(id="sb_form_go")
	public WebElement searchButton;
	
	@FindBy(id="b_results")
	public WebElement resultsCount;
	
	public void enterSearchText(String searchText) throws Exception {
	
		enterText(searchBox, searchText, "Search Box");
	}
	
	public void performSearch() throws Exception {
		click(searchButton, "Search Button");
	}
	
	public void verifySearchResults() throws Exception {
		//Verify if search results are displayed
		isElementDisplayed(resultsCount, "Bing search results");
	}
		
}

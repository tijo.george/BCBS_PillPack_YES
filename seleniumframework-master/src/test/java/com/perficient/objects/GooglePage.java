package com.perficient.objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.perficient.util.CommonUtilities;

public class GooglePage extends CommonUtilities {

	public GooglePage(RemoteWebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	@FindBy(name="query")
	public WebElement searchBox;

	@FindBy(xpath="//button[@class='btn btn-default']")
	public WebElement searchButton;
	
	@FindBy(xpath="//a[@href='books/selenium-webdriver-book']")
	public WebElement booklink;
	
	@FindBy(xpath="//input[@value='add to cart']")
	public WebElement addToCartBtn;
	
	@FindBy(xpath="//a[@href='/bookstore/cart']")
	public WebElement cartBtn;
	
	
	public void addBookToCart(String searchText) throws Exception {
		enterText(searchBox, searchText, "Serach Box");
		click(searchButton, "Search Button");
		click(booklink, "Book Link");
		click(addToCartBtn, "Add to CartButton");
		click(cartBtn, "Cart Button");
	}
}

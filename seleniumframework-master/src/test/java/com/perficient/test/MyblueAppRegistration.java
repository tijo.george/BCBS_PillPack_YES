/**
 * A sample test script. 
 * @author Srinivasan Ramasamy
 * @version 1.0
 */

package com.perficient.test;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import com.perficient.core.TestDriver;
import com.perficient.objects.GooglePage;
import com.perficient.objects.MyBlueAppPageelements;

public class MyblueAppRegistration extends TestDriver{
	
	
	@Test (dataProvider="data-provider", dataProviderClass=TestDriver.class, enabled=true, priority=1)
	public void myblueAppRegistration() {
		try {
			
			MyBlueAppPageelements obj = new MyBlueAppPageelements(appiumDriver);
			obj.testUserRegistration(data.get("UserName"),data.get("Password"),data.get("FirstName"),data.get("LastName"),
					data.get("DOB"),data.get("CardMemId"),data.get("SSN"));
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}

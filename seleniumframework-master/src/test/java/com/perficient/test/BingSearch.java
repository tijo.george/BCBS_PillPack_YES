/**
 * A sample test script. 
 * @author Srinivasan Ramasamy
 * @version 1.0
 */

package com.perficient.test;

import org.testng.annotations.Test;
import com.perficient.core.TestDriver;
import com.perficient.objects.BingPage;

public class BingSearch extends TestDriver{
	

	@Test (dataProvider="data-provider", dataProviderClass=TestDriver.class, enabled=true)
	public void bingSearch() {
		try {
			BingPage obj = new BingPage(driver);
			obj.open(data.get("URL"));
			obj.enterSearchText(data.get("SearchString"));
			obj.performSearch();
			obj.verifySearchResults();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
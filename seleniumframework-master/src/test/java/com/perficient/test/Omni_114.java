/**
 * A sample test script. 
 * @author Srinivasan Ramasamy
 * @version 1.0
 */

package com.perficient.test;

import org.testng.annotations.Test;

import com.perficient.core.TestDriver;
import com.perficient.objects.MyBlueLoginPageElements;

public class Omni_114 extends TestDriver{
	

	@Test (dataProvider="data-provider", dataProviderClass=TestDriver.class, enabled=true)
	public void omni_114() {
		try {
			MyBlueLoginPageElements obj = new MyBlueLoginPageElements(driver);
			obj.open(data.get("URL"));
			obj.login(data.get("UserName"),data.get("Password"));
			obj.verifyPPPromoLink();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
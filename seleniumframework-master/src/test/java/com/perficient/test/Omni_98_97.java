/**
 * A sample test script. 
 * @author Srinivasan Ramasamy
 * @version 1.0
 */

package com.perficient.test;

import org.testng.annotations.Test;

import com.perficient.core.TestDriver;
import com.perficient.objects.MyBlueLoginPageElements;
import com.perficient.objects.MyPillPackPageElements;

public class Omni_98_97 extends TestDriver{
	

	@Test (dataProvider="data-provider", dataProviderClass=TestDriver.class, enabled=true)
	public void omni_98_97() {
		try {
			MyBlueLoginPageElements obj = new MyBlueLoginPageElements(driver);
			MyPillPackPageElements ppPage = new MyPillPackPageElements(driver);
			obj.open(data.get("URL"));
			obj.login(data.get("UserName"),data.get("Password"));
			driver.navigate().to(data.get("My_PillPack_url"));
			ppPage.verify_Allergy_HealthInfo(data.get("Allergy"),data.get("HealthCondition"));
			ppPage.verify_Pregnancy_info();	
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
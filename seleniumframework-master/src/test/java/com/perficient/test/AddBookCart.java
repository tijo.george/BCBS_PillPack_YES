/**
 * A sample test script. 
 * @author Srinivasan Ramasamy
 * @version 1.0
 */

package com.perficient.test;

import org.testng.annotations.Test;
import com.perficient.core.TestDriver;
import com.perficient.objects.GooglePage;

public class AddBookCart extends TestDriver{
	
	
	@Test (dataProvider="data-provider", dataProviderClass=TestDriver.class, enabled=true, priority=1)
	public void addBookCart() {
		try {
			GooglePage obj = new GooglePage(driver);
			obj.open(data.get("URL"));
			obj.addBookToCart(data.get("SearchString"));
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}

/**
 * TestNG class which gets called for every test case. 
 * This is the driver class which runs for each test case. 
 * It reads the corresponding testdata from the input sheet and runs the actual test method 
 * and writes back the test results to the output sheet.
 * 
 */

package com.perficient.core;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.util.LinkedHashMap;
import java.util.Properties;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import com.perficient.util.BrowserUtilities;
import com.perficient.util.ExtentManager;
import com.perficient.util.IOUtil;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class TestDriver  {

	public  LinkedHashMap<String, String> data;
	public RemoteWebDriver driver;
	public AppiumDriver appiumDriver;
	public static ExtentReports extent;
	public static ExtentTest test;
	public static Properties props;
	public String databaseName;
	public String databaseURL;
	public String databaseUsername;
	public String databasePassword;
	public static String appium_port;
	public static String appName;
	public static String udid;
	public static String appPackage;
	public static String appActivity;
	int intCurrentIteration = -1;
	String nodeUrl = null;

	// BeforeSuite sets up the configuration file and sets up the excel & html
	// output files
	@Parameters("grid")
	@BeforeSuite  
	public void suiteSetup(String grid) throws IOException {
		File file = new File("user_config.properties");
		FileReader reader = new FileReader(file);
		props = new Properties();
		props.load(reader);
		extent = ExtentManager.Instance();
		if (grid.equals("true")) {
			nodeUrl = props.getProperty("nodeUrl");
		}
		databaseName = props.getProperty("databaseName");
		databaseURL = props.getProperty("databaseURL");
		databaseUsername = props.getProperty("databaseUsername");
		databasePassword = props.getProperty("databasePassword");
		appName = props.getProperty("appName");
		udid = props.getProperty("udid");
		appPackage = props.getProperty("appPackage");
		appActivity = props.getProperty("appActivity");
	}

	@BeforeMethod
	public void InitializeTestIteration( ITestContext context) throws MalformedURLException {
		BrowserUtilities browser=new BrowserUtilities();
		System.out.println(this.getClass().getName());
		for (int i = 0; i < context.getAllTestMethods().length; i++) {
			if (this.getClass().getName().split("test.")[1].equalsIgnoreCase(context.getAllTestMethods()[i].getMethodName())) {
				intCurrentIteration = context.getAllTestMethods()[i].getCurrentInvocationCount() + 1;
			}
		}
		if (props.getProperty("whichinputfiletouse").equalsIgnoreCase("excel"))
			data = IOUtil.getInputData(this.getClass().getName().split("test.")[1], intCurrentIteration);
		else if (props.getProperty("whichinputfiletouse").equalsIgnoreCase("json"))
			data = IOUtil.getInputDataFromJSON(this.getClass().getName().split("test.")[1], intCurrentIteration);
		System.out.println(data);
		//driver = browser.getBrowser(context.getCurrentXmlTest().getParameter("browsers"), nodeUrl);
		test = ExtentManager.startTest(data.get("TC_Name") + " [Iteration" + Integer.toString(intCurrentIteration) + "]",
				data.get("TC_Description") + " [Broswer : " + context.getCurrentXmlTest().getParameter("browsers")
				+ "]");
	}

	@BeforeClass
	public void InitializeTestIterationClass( ITestContext context) throws Exception {
		BrowserUtilities browser=new BrowserUtilities();
		if( context.getCurrentXmlTest().getParameter("toolName").equalsIgnoreCase("Selenium"))
				driver = browser.getBrowser(context.getCurrentXmlTest().getParameter("browsers"), nodeUrl);
		else {
			//startAppiumServer(udid);
			appiumDriver= browser.launchApp(appium_port);
		}
	}

	
	@DataProvider(name = "data-provider")
	public static Object[][] dataProvider(Method result) {
		int iterationCount;
		if (props.getProperty("whichinputfiletouse").equalsIgnoreCase("json"))
			iterationCount = IOUtil.getNumOfChildNodesFromJSON(result.getName());
		else
			iterationCount = IOUtil.getNumOfChildNodesFromExcel(result.getName());
		System.out.println(iterationCount);
		Object[][] obj = new Object[iterationCount][0];
		return obj;
	}

	// All the tear down operations occur. Since Reporting is done at the end of
	// every test method
	@AfterClass
	public void tearDown() {
		if (driver != null)
			driver.quit();
		//stopServer();
		//ExtentManager.endTest();
		data.clear();
	}
	
	@AfterMethod
	public void tearDownMethod() {
		/*if (driver != null)
			driver.quit();*/
		ExtentManager.endTest();
		data.clear();
	}

	// The extent reporting is done for each suite. The extent/HTML report is
	// flushed and closed
	@AfterSuite
	public void flush() {
		if (driver != null)
		driver.quit();

		ExtentManager.extent.flush();
		//extent.close();
	}
	
	public void startAppiumServer(String udid) throws Exception {
		String chromePort = getPort();
		appium_port = getPort();
        String bootStrapPort = getPort();
        String nodePath_windows = props.getProperty("nodePath_windows");
        String appiumJSPath_windows = props.getProperty("appiumJSPath_windows");
        System.out.println("nodePath_windows" + nodePath_windows);
        System.out.println("appiumJSPath_windows" + appiumJSPath_windows);
        CommandLine command = new CommandLine(nodePath_windows);
        command.addArgument(appiumJSPath_windows, false);
        command.addArgument("--address", false);
        command.addArgument("127.0.0.1");
        command.addArgument("--port", false);
        command.addArgument(appium_port);
        command.addArgument("--no-reset", false);
        command.addArgument("--session-override", false);
        command.addArgument("-U", false);
        command.addArgument(udid);
        command.addArgument("--bootstrap-port", false);
        command.addArgument(bootStrapPort);
        command.addArgument("--chromedriver-port", false);
        command.addArgument(chromePort);
        DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
        DefaultExecutor executor = new DefaultExecutor();
        executor.setExitValue(1);
                        System.out.println("appium server" + appiumJSPath_windows);
                        executor.execute(command, resultHandler);
                       Thread.sleep(10000);

        } 
	

	 public String getPort() throws Exception {
         ServerSocket socket = new ServerSocket(0);
         socket.setReuseAddress(true);
         String port = Integer.toString(socket.getLocalPort());
         socket.close();
         return port;
	 }
	 
	 public static void stopServer() {
         try {
                 System.out.println("Stop server");
                 String filePath = "";
                 String filePath1 = "";
                 filePath = "taskkill /F /IM node.exe";
                 Runtime.getRuntime().exec(filePath);
                 filePath1 = "taskkill /F /IM chromedriver.exe ";
                 Runtime.getRuntime().exec(filePath1);
         } catch (Exception e) {
            e.printStackTrace();
         }
	 }

}

package com.perficient.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.perficient.core.TestDriver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.github.bonigarcia.wdm.WebDriverManager;

public class BrowserUtilities {

	public RemoteWebDriver driver;
	public  AppiumDriver appiumDriver;

	// derive the required Browser
	public  RemoteWebDriver getBrowser(String browserName, String nodeUrl) throws MalformedURLException {
		if (browserName.equalsIgnoreCase("FF") || browserName.equalsIgnoreCase("Firefox")) {
			driver = getFirefoxBrowser(nodeUrl);
		} else if (browserName.equalsIgnoreCase("IE") || browserName.equalsIgnoreCase("InternetExplorer")) {
			driver = getInternetExplorerDriver(nodeUrl);
		} else if (browserName.equalsIgnoreCase("Chrome")) {
			driver = getChromeDriver(nodeUrl);
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Integer.parseInt(TestDriver.props.getProperty("timeout")),
				TimeUnit.SECONDS);
		
		return driver;
		
	}
	
	public AppiumDriver launchApp(String port) throws MalformedURLException {
		String appiumURL = "http://127.0.0.1:" + 4723 + "/wd/hub";
		DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("appium-version", "1.12");
        //capabilities.setCapability("app", TestDriver.appName);
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", TestDriver.udid);
        capabilities.setCapability("udid", TestDriver.udid);
        capabilities.setCapability("noSign", true);
        capabilities.setCapability("noReset", true);
        capabilities.setCapability("automationName", "uiautomator2");
        capabilities.setCapability("appPackage", TestDriver.appPackage);
        capabilities.setCapability("appActivity",TestDriver.appActivity);
        appiumDriver = new AndroidDriver(new URL(appiumURL), capabilities);
        appiumDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        return appiumDriver;
	}

	// load FF browser and enable the logs
	public  RemoteWebDriver getFirefoxBrowser(String nodeUrl) throws MalformedURLException {
		new DesiredCapabilities();
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();

		LoggingPreferences logs = new LoggingPreferences();
		logs.enable(LogType.BROWSER, Level.ALL);
		logs.enable(LogType.CLIENT, Level.ALL);
		logs.enable(LogType.DRIVER, Level.INFO);
		logs.enable(LogType.PERFORMANCE, Level.ALL);
		logs.enable(LogType.PROFILER, Level.ALL);
		logs.enable(LogType.SERVER, Level.ALL);
		capabilities.setCapability(CapabilityType.LOGGING_PREFS, logs);

		WebDriverManager.firefoxdriver().setup();
		if (nodeUrl != null)
			driver = new RemoteWebDriver(new URL(nodeUrl), capabilities);
		else
			driver = new FirefoxDriver(capabilities);
		return driver;
	}

	// load IE browser and enable the logs
	public  RemoteWebDriver getInternetExplorerDriver(String nodeUrl) throws MalformedURLException {
		new DesiredCapabilities();
		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

		LoggingPreferences logs = new LoggingPreferences();
		logs.enable(LogType.BROWSER, Level.ALL);
		logs.enable(LogType.CLIENT, Level.ALL);
		logs.enable(LogType.DRIVER, Level.INFO);
		logs.enable(LogType.PERFORMANCE, Level.ALL);
		logs.enable(LogType.PROFILER, Level.ALL);
		logs.enable(LogType.SERVER, Level.ALL);
		capabilities.setCapability(CapabilityType.LOGGING_PREFS, logs);
		capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
		capabilities.setJavascriptEnabled(true);
		//WebDriverManager.iedriver().setup();
		System.setProperty("webdriver.ie.driver", "src/test/resources/drivers/IEDriverServer.exe");
		if (nodeUrl != null)
			driver = new RemoteWebDriver(new URL(nodeUrl), capabilities);
		else
			driver = new InternetExplorerDriver(capabilities);
		return driver;
	}

	// load Chrome browser and enable the logs
	public  RemoteWebDriver getChromeDriver(String nodeUrl) throws MalformedURLException {
		new DesiredCapabilities();
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

		ChromeOptions options = new ChromeOptions();
		options.addArguments("--test-type");
		options.addArguments("--no-sandbox");
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);

		LoggingPreferences logs = new LoggingPreferences();
		logs.enable(LogType.BROWSER, Level.ALL);
		logs.enable(LogType.DRIVER, Level.INFO);
		logs.enable(LogType.PERFORMANCE, Level.ALL);
		capabilities.setCapability(CapabilityType.LOGGING_PREFS, logs);

		//WebDriverManager.chromedriver().setup();
		System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
		if (nodeUrl != null)
			driver = new RemoteWebDriver(new URL(nodeUrl), capabilities);
		else
			driver = new ChromeDriver(capabilities);
		return driver;
	}

	// load PhantomJS browser and enable the logs
	
}

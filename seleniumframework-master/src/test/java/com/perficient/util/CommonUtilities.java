package com.perficient.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.AppiumDriver;

public class CommonUtilities {

	Actions builder = null;
	public RemoteWebDriver driver;
	public  AppiumDriver appiumDriver;
	public static WebDriverWait wait;
	public static WebDriverWait waitAppium;
	public String toolName;
	
	public CommonUtilities(RemoteWebDriver driver) {
		this.driver=driver;
		toolName="Selenium";
		wait=new WebDriverWait(driver,40);
	}
	
	public CommonUtilities(AppiumDriver driver) {
		this.appiumDriver=driver;
		toolName="Appium";
		waitAppium =new WebDriverWait(appiumDriver,40);
				}

	// Get the screenshot
	public String getScreenshot() throws Exception {
		Calendar cal = Calendar.getInstance();
        long s = cal.getTimeInMillis();
        File srcfile;
        if (toolName.equalsIgnoreCase("Selenium"))	 
        	 srcfile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		 else
			 srcfile = ((TakesScreenshot) appiumDriver).getScreenshotAs(OutputType.FILE);
		File destfile = new File("ReportGenerator/"+ExtentManager.reportFolder+"/Screenshot/imgae_"+s+".png");
		FileUtils.copyFile(srcfile, destfile);
		return destfile.getAbsolutePath();
	}

	// Log the test step report to the corresponding test case
	public void report(String Status, String Description, boolean isScreenshotNeeded) throws Exception {
		String sspath="";
		if (isScreenshotNeeded) {
			sspath=ExtentManager.getTest().addScreenCapture(getScreenshot());
		}
		switch (Status.toUpperCase()) {
		case "PASS":
			ExtentManager.getTest().log(LogStatus.PASS, Description,sspath);
			break;
		case "FAIL":
			ExtentManager.getTest().log(LogStatus.FAIL, Description,sspath);
			Assert.fail(Description);
			break;
		case "WARNING":
			ExtentManager.getTest().log(LogStatus.WARNING, Description,sspath);
			break;
		case "INFO":
			ExtentManager.getTest().log(LogStatus.INFO, Description,sspath);
			break;
		case "ERROR":
			ExtentManager.getTest().log(LogStatus.ERROR, Description,sspath);
			Assert.fail(Description);
			break;
		default:
			ExtentManager.getTest().log(LogStatus.UNKNOWN, Description,sspath);
			break;
		}
	}

	// Open a URL in the driver's browser
	public void open(String strURL) throws Exception {
		driver.get(strURL);
		//ExtentManager.getTest().log(LogStatus.PASS,  String.format("Navigated to page - %s", strURL), ExtentManager.getTest().addScreenCapture(getScreenshot()));
		report("INFO", String.format("Navigated to page - %s", strURL), true);
	}

	// Wait for parameterized amount of seconds
	public void waitFor(long waitSeconds) {
		try {
			Thread.sleep(waitSeconds * 1000);
		} catch (Exception e) {
		}
	}

	// Switch to the second window
	public void switchToNewWindow() throws Exception {
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
	}

	// Switch back to the first window
	public void switchToOldWindow() throws Exception {
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.close();
		driver.switchTo().window(tabs.get(0));
	}

	// Switch to the frame using it's name
	public void switchToiframe(String frameName) throws Exception {
		driver.switchTo().frame(frameName);
		waitFor(3);
	}

	// Get the WebElement using the parameterized locator
	public WebElement getElement(By by) {
		WebElement ele = null;
		try {
			ele = driver.findElement(by);
		} catch (Exception e) {
		}
		return ele;
	}

	// Get the Select WebElement using the parameterized locator
	public Select getDropdown(By by) throws Exception {
		return new Select(getElement(by));
	}

	// Click on a WebElement
	public boolean click(WebElement ele) {
		try {
			ele.click();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public void click(WebElement element,String elementName) throws Exception {
		 try {
		 if (toolName.equalsIgnoreCase("Selenium"))	 
			 wait.until(ExpectedConditions.elementToBeClickable(element));
		 else
			 waitAppium.until(ExpectedConditions.elementToBeClickable(element));
		 element.click();
		 report("PASS", elementName+ " is clicked successfully", true);
		 }catch(Exception e) {
			 System.out.println(element+ " not visible on the page");
			 report("FAIL", elementName+ " is not clicked ", true);
			 throw new Exception(element+ " is not clicked");
		 }
	}

	public void enterText(WebElement element, String text,String elementName) throws Exception {
		try {
			 if (toolName.equalsIgnoreCase("Selenium"))	 
				 wait.until(ExpectedConditions.elementToBeClickable(element));
			 else
				 waitAppium.until(ExpectedConditions.elementToBeClickable(element));
		 element.clear();
		 element.click();
		 element.sendKeys(text);
		 report("PASS", "EnterText on "+elementName+" is successfull", true);
		}catch(Exception e) {
			 System.out.println(element+ " not visible on the page");
			 report("FAIL", element+ " not visible on the page", true);
			 throw new Exception(element+ " not visible on the page");
		}
	}
	
	public void selectValueDropDown(WebElement element, String value,String elementName) throws Exception {
		try {
			 if (toolName.equalsIgnoreCase("Selenium"))	 
				 wait.until(ExpectedConditions.elementToBeClickable(element));
			 else
				 waitAppium.until(ExpectedConditions.elementToBeClickable(element));
		 Select sl = new Select(element);
        sl.selectByVisibleText(value);
        report("PASS", "Selecting"+value+" from "+elementName+" is successfull",true);
		}catch(Exception e) {
			 System.out.println("Dropdown value selection failed");
			 report("FAIL","Dropdown value selection failed",true);
			 throw new Exception("Dropdown value selection failed");
		}
	}
	

	public boolean click(By by) {
		try {
			getElement(by).click();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// Type into a WebElement
	public boolean type(WebElement ele, String strText) {
		try {
			ele.sendKeys(strText);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean type(By by, String strText) {
		try {
			getElement(by).sendKeys(strText);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// Select from a drop-down using Visible Text
	public boolean selectDropdown_ByVisibleText(Select ele, String strVisibleText) {
		try {
			ele.selectByVisibleText(strVisibleText);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean selectDropdown_ByVisibleText(By by, String strVisibleText) {
		try {
			getDropdown(by).selectByVisibleText(strVisibleText);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// Select from a drop-down using it's Index
	public boolean selectDropdown_ByIndex(Select ele, int index) {
		try {
			ele.selectByIndex(index);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean selectDropdown_ByIndex(By by, int index) {
		try {
			getDropdown(by).selectByIndex(index);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// Select from a drop-down using it's Value
	public boolean selectDropdown_ByValue(Select ele, String value) {
		try {
			ele.selectByValue(value);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean selectDropdown_ByValue(By by, String value) {
		try {
			getDropdown(by).selectByValue(value);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean isElementDisplayed(WebElement element, String elementname) throws Exception {
		try {
			wait.until(ExpectedConditions.visibilityOf(element));
			report("PASS", elementname+ " is visible on the screen", true);
			return true;
		}catch(Exception e) {
			report("Page", elementname+ " is not visible on the page", true);
			throw new Exception(e);
		}
	}
	// Verify if the text in an element is empty, will still continue with the test
	// case execution
	public void verifyIfTextIsNotEmpty(WebElement ele, String strMessage) throws Exception {
		if (ele.getText().trim() == null || ele.getText().trim().length() <= 0)
			report("PASS", "PASSED : " + strMessage, false);
		else
			report("ERROR", "ERROR : " + strMessage, true);
	}

	public void verifyIfTextIsNotEmpty(By by, String strMessage) throws Exception {
		if (getElement(by) == null)
			report("ERROR", "ERROR : " + strMessage + " (Element not available)", true);
		else if (getElement(by).getText().trim() == null || getElement(by).getText().trim().length() <= 0)
			report("PASS", "PASSED : " + strMessage, false);
		else
			report("ERROR", "ERROR : " + strMessage, true);
	}

	// Assert if the text in an element is empty, will stop the current test case
	// execution
	public void assertIfTextIsNotEmpty(WebElement ele, String strMessage) throws Exception {
		if (ele.getText().trim() == null || ele.getText().trim().length() <= 0)
			report("PASS", "PASSED : " + strMessage, false);
		else
			report("FAIL", "FAILED : " + strMessage, true);
	}

	public void assertIfTextIsNotEmpty(By by, String strMessage) throws Exception {
		if (getElement(by) == null)
			report("FAIL", "FAILED : " + strMessage + " (Element not available)", true);
		else if (getElement(by).getText().trim() == null || getElement(by).getText().trim().length() <= 0)
			report("PASS", "PASSED : " + strMessage, false);
		else
			report("FAIL", "FAILED : " + strMessage, true);
	}

	// Verify the text in an element against the parameterized text, will still
	// continue with the test case execution
	public void verifyElementText(WebElement ele, String strExpectedText) throws Exception {
		String strActualText = ele.getText();

		if (strActualText.equalsIgnoreCase(strExpectedText))
			report("PASS", "PASSED : The expected text '" + strExpectedText + "' is displayed on the screen", false);
		else
			report("ERROR", "ERROR : The expected text '" + strExpectedText + "' is not displayed on the screen", true);
	}

	public void verifyElementText(By by, String strExpectedText) throws Exception {
		if (getElement(by) == null) {
			report("ERROR",
					"ERROR : The expected text '" + strExpectedText + "' is not displayed (Element not available)",
					true);
			return;
		}

		String strActualText = getElement(by).getText();

		if (strActualText.equalsIgnoreCase(strExpectedText))
			report("PASS", "PASSED : The expected text '" + strExpectedText + "' is displayed on the screen", false);
		else
			report("ERROR", "ERROR : The expected text '" + strExpectedText + "' is not displayed on the screen", true);
	}

	// Assert the text in a WebElement against the parameterized text, will stop the
	// current test case execution
	public void assertElementText(WebElement ele, String strExpectedText) throws Exception {
		String strActualText = ele.getText();

		if (strActualText.equalsIgnoreCase(strExpectedText))
			report("PASS", "PASSED : The expected text '" + strExpectedText + "' is displayed on the screen", false);
		else
			report("FAIL", "FAIL : The expected text '" + strExpectedText + "' is not displayed on the screen", true);
	}

	public void assertElementText(By by, String strExpectedText) throws Exception {
		if (getElement(by) == null) {
			report("FAIL",
					"FAILED : The expected text '" + strExpectedText + "' is not displayed (Element not available)",
					true);
			return;
		}

		String strActualText = getElement(by).getText();

		if (strActualText.equalsIgnoreCase(strExpectedText))
			report("PASS", "PASSED : The expected text '" + strExpectedText + "' is displayed on the screen", false);
		else
			report("FAIL", "FAILED : The expected text '" + strExpectedText + "' is not displayed on the screen", true);
	}

	// Verify if the WebElement exists, will still continue with the test case
	// execution
	public void verifyPresence(By by, String strMessage) throws Exception {
		if (getElement(by) != null)
			report("PASSED", "PASSED : " + strMessage, true);
			//ExtentManager.getTest().log(LogStatus.PASS, "PASSED : " + strMessage, ExtentManager.getTest().addScreenCapture(getScreenshot()));
		else
			//ExtentManager.getTest().log(LogStatus.FAIL, "Failed : " + strMessage, ExtentManager.getTest().addScreenCapture(getScreenshot()));
			report("ERROR", "ERROR : " + strMessage, true);
	}

	// Assert if the WebElement exists, will stop the current test case execution
	public void assertPresence(By by, String strMessage) throws Exception {
		if (getElement(by) != null)
			report("PASS", "PASSED : " + strMessage, false);
		else
			report("FAIL", "FAILED : " + strMessage, true);
	}

	public WebElement waitUntilElementPresent(By by) throws Exception {
		return new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(by));
	}

	public boolean waitUntilElementInvisible(By by) throws Exception {
		return new WebDriverWait(driver, 30).until(ExpectedConditions.invisibilityOfElementLocated(by));
	}

	// Navigate To Menu
	public boolean goToMenu(By mainMenu, By... menuItem) {
		boolean actionPerformed = false;
		try {
			Actions action = new Actions(driver);
			WebElement Mainmenu = getElement(mainMenu);
			if (menuItem.length > 0) {
				action.moveToElement(Mainmenu).build().perform();
				for (int i = 0; i < menuItem.length; i++) {
					By css = menuItem[i];
					if (i == menuItem.length - 1)
						action.moveToElement(new WebDriverWait(driver, 2)
								.until(ExpectedConditions.visibilityOfElementLocated(css))).click().build().perform();
					else
						action.moveToElement(new WebDriverWait(driver, 2)
								.until(ExpectedConditions.visibilityOfElementLocated(css))).build().perform();
				}
				actionPerformed = true;
			} else {
				action.moveToElement(Mainmenu).build().perform();
				actionPerformed = true;
			}
		} catch (Exception ex) {
			actionPerformed = false;
		}

		// If actionPerformed is true then the navigation was successful, else the
		// navigation failed
		return actionPerformed;
	}

	public void scrollToTop() throws Exception {
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
	}

	public void scrollToBottom() throws Exception {
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

	public void scrollInToView(WebElement ele) throws Exception {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ele);
	}

	public void scrollInToView(By by) throws Exception {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", getElement(by));
	}

	public String getRandomName() throws Exception {
		String randomValue = Integer.toString((int) (Math.random() * 1000000000));
		return "Test_" + randomValue.substring(0, 5);
	}

	public int randomNumber(int limit) {
		return new Random().nextInt(limit) + 1;
	}

	public boolean doubleClick(By by) {
		try {
			builder = new Actions(driver);
			builder.doubleClick(getElement(by)).build().perform();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean clickAndDrag(WebElement source, WebElement target) {
		try {
			builder = new Actions(driver);
			builder.dragAndDrop(source, target).build().perform();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean clickAndDrag(WebElement source, int xOffset, int yOffset) throws Exception {
		try {
			builder = new Actions(driver);
			builder.dragAndDropBy(source, xOffset, yOffset).build().perform();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
package com.perficient.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.openqa.selenium.remote.RemoteWebDriver;

public class DatabaseConnection  extends CommonUtilities{
	
		public DatabaseConnection(RemoteWebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

		// Object of Connection from the Database
		private static Connection dbConnect;
	public	TreeMap<String,ArrayList<Map<String, Object>>> selectResult =  new TreeMap<String,ArrayList<Map<String, Object>>>();                   
		
		public void dbConnection(String db, String dbURL,String user,String password) throws SQLException, ClassNotFoundException{ 
			
			//Loading the required JDBC Driver class
			switch(db)
			{
			case "oracle":
				Class.forName("oracle.jdbc.driver.OracleDriver"); //Jar has to be added manually for Oracle Database
				break;
			case "mysql":
				Class.forName("com.mysql.jdbc.Driver");
				break;
			case "sqlserver":
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				break;
			}
			
			dbConnect = DriverManager.getConnection(dbURL, user, password);
		}	
		
		  public TreeMap<String,ArrayList<Map<String,Object>>> 
		    executeSelectQueries(String...selectQuery) throws Exception 
		    { /* This is a generic method for executing the given Select Queries.
		         Applicable only for Select Queries */
			  

		        String[] keys = selectQuery;
		        try {
		        	for (int i=0; i<keys.length; i++) { 
		            PreparedStatement stmt = dbConnect.prepareStatement(keys[i]);
		            //rs is the resultset for each query
		            ResultSet rs = stmt.executeQuery();
		            //Fetch the metadata of the result
		            ResultSetMetaData rsmd = rs.getMetaData();
		            //Get the column Count of the resultSet
		            int columncount = rsmd.getColumnCount();
		            
		            ArrayList<Map<String, Object>> queryResult = new ArrayList<Map<String, Object>>();
		            
		            while (rs.next()) {
		            	Map<String, Object> row = new HashMap<String, Object>();
		                for (int j = 1; j <= columncount; j++) {
		                    row.put(rsmd.getColumnName(j),rs.getObject(j));
		            } 
		            queryResult.add(row);
		        	}
		            if (queryResult.isEmpty())
		            {
		            	report("WARNING", " WARNING : 0 row(s) affected for the given query " + keys[i] , false);
		            }
		            selectResult.put(keys[i],queryResult);
		        }
		        }
		         catch(SQLException sqlExc) {
		        	 System.out.println(sqlExc.getMessage());
		        	 report("FAIL", sqlExc.getMessage() , false);
		        } 
		        return selectResult;
		    } 
		  
		  /* This method is to check for a specific value in a specific column in the Resultset
		   *  got by executing the Select Queries using executeSelectQueries() method. */
		  public void checkForAValueInResultSeT(String query,String columnName ,String value) throws Exception {
			  
			  ArrayList<Map<String, Object>> resultSetFortheQuery   = selectResult.get(query);
			  boolean valueIsPresent = false;
			  
			  for(int i=0 ;i<=resultSetFortheQuery.size()-1; i++)
			  {
				  Map<String, Object> resultRow = resultSetFortheQuery.get(i);
				  
				  if(resultRow.containsKey(columnName))
				  {
				  if (resultRow.containsValue(value)) {
					  valueIsPresent = true;
					  report("PASS", "PASSED :  Result for the Query " +query+ " contains the given key " + " ' " + columnName+ " ' " + "with  value " + " ' " +value + " ' " , false);
					  break;
				  }
			  }
			  }
			  if (!valueIsPresent)
			  { 
				  report("WARNING", "WARNING : Result for the Query " +query+ " does not contain the given "+"'" + columnName +"'" + "with value " +"'" +value + "'" , false);
			  }
		  }
		  
		  public void executeDMLQueries(String...DMLQuery) throws Exception {
			   /* This is a generic method for executing the given INSERT or UPDATE or DELETE Queries.
			    * Applicable only for INSERT or UPDATE or DELETE Queries */
				  
			  String[] keys = DMLQuery;
			  try {
		        	for (int i=0; i<keys.length; i++) { 
		            PreparedStatement stmt = dbConnect.prepareStatement(keys[i]);
		            int rs = stmt.executeUpdate();
		            
		            if (rs >= 1)
		            	report("PASS", "PASSED: " + rs + " row(s) affected for the given query " + keys[i]  , false);
		            else
		            	report("WARNING","WARNING: " + rs + " row(s) affected for the given query " + keys[i] , false);
		        	}
			  }
		            catch(SQLException sqlExc) {
			           System.out.println(sqlExc.getMessage());
			           report("FAIL", sqlExc.getMessage() , false);
			        } 
			  }
		 		 
		  public void dbClose() throws SQLException {
			  dbConnect.close();
		    }

}


	
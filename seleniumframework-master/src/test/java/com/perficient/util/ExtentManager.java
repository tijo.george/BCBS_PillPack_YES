package com.perficient.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class ExtentManager {

	public static ExtentReports extent ;
    public static String reportFolder ="";
    static Map extentTestMap = new HashMap();


	// Set up of the Extent Report
    public synchronized static ExtentReports Instance() throws IOException
	{
		 SimpleDateFormat sdfDateReport = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");

			Date now = new Date();
			reportFolder = "HtmlReport_" + sdfDateReport.format(now);
			String s=new File("ReportGenerator/"+reportFolder+"/TestReport.html").getPath();
			extent = new ExtentReports(s, true,Locale.ENGLISH);
			extent.loadConfig(new File ("extentreports_config.xml"));
			extent.addSystemInfo("Selenium Version", "3.12");
			extent.addSystemInfo("Environment", "Production");
			extent.assignProject("EHealth");
			
			Path temp = Files.copy 
			        (Paths.get("src/test/resources/Myblue_logo.PNG"),  
			        Paths.get("ReportGenerator/"+reportFolder+"/Myblue_logo.PNG"));

	return extent;
}
	public static synchronized ExtentTest getTest() {
        return (ExtentTest) extentTestMap.get((int) (long) (Thread.currentThread().getId()));
    }

    public static synchronized void endTest() {
        extent.endTest((ExtentTest) extentTestMap.get((int) (long) (Thread.currentThread().getId())));
    }

    public static synchronized ExtentTest startTest(String testName) {
        return startTest(testName, "");
    }

    public static synchronized ExtentTest startTest(String testName, String desc) {
        ExtentTest test = extent.startTest(testName, desc);
        extentTestMap.put((int) (long) (Thread.currentThread().getId()), test);
        return test;
    }
	
}